package co.md.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import co.md.model.MDClickORImpression;

/**
 * MDClickOrImpressionRepository is used to work with click or impression data 
 * @author Santosh
 *
 */
@Repository("JpaRepository<MDClickORImpression, Long>")
public interface MDClickOrImpressionRepository  extends  JpaRepository<MDClickORImpression, Long>{

}
