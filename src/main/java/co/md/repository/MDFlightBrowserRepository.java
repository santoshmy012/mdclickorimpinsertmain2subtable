package co.md.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.md.model.MDFlightBrowser;
@Repository("jpaRepository<MDFlightBrowser, Long>")
public interface MDFlightBrowserRepository extends JpaRepository<MDFlightBrowser, Long> {
}
