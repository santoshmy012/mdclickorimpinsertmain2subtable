package co.md.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.md.model.MDFlightCreative;

@Repository("JpaRepository<MDFlightCreative, Long>")
public interface MDFlightCreativeRepository extends JpaRepository<MDFlightCreative, Long> {
}
