package co.md.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.md.model.MDFlightOverview;

@Repository("JpaRepository<MDFlightOverview, Long>")
public interface MDFlightOverviewRepository extends JpaRepository<MDFlightOverview, Long> {}
