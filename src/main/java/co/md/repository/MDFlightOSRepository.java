package co.md.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.md.model.MDFlightOS;

@Repository("JpaRepository<MDFlightOS, Long>")
public interface MDFlightOSRepository extends JpaRepository<MDFlightOS, Long> {}
