package co.md.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.md.model.MDFlightSource;
@Repository("JpaRepository<MDFlightSource, Long>")
public interface MDFlightSourceRepository extends JpaRepository<MDFlightSource, Long> {}
