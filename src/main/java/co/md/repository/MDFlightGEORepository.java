package co.md.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.md.model.MDFlightGEO;

/**
 * This MDFlightGEORepository repository  is only provide the service from the database for GEO of publisher  only.
 * @author Santosh
 *
 */
@Repository("jpaRepository")
public interface MDFlightGEORepository extends JpaRepository<MDFlightGEO, Long>{

}
