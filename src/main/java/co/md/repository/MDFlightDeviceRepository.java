package co.md.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import co.md.model.MDFlightDevice;
@Repository("JpaRepository<MDFlightDevice, Long>")
public interface MDFlightDeviceRepository extends JpaRepository<MDFlightDevice, Long> {}
