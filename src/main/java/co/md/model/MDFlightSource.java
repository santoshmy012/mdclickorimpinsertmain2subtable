package co.md.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "report_mdflight_source_display")
public class MDFlightSource {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name = "mdfid")
	private Long mdFId;
	@Column(name = "source")
	private String mdSource;
	@Column(name = "clicks")
	private Long mdClicks;
	@Column(name = "impressions")
	private Long mdImpressions;
	@Column(name = "payout")
	private Double mdPayout;
	@Column(name = "revenuecost")
	private Double revenueCost;
	@Column(name = "date")
	private Date date;
	@Column(name = "hour")
	private String hour;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getMdFId() {
		return mdFId;
	}

	public void setMdFId(Long mdFId) {
		this.mdFId = mdFId;
	}

	public String getMdSource() {
		return mdSource;
	}

	public void setMdSource(String mdSource) {
		this.mdSource = mdSource;
	}

	public Long getMdClicks() {
		return mdClicks;
	}

	public void setMdClicks(Long mdClicks) {
		this.mdClicks = mdClicks;
	}

	public Long getMdImpressions() {
		return mdImpressions;
	}

	public void setMdImpressions(Long mdImpressions) {
		this.mdImpressions = mdImpressions;
	}

	public Double getMdPayout() {
		return mdPayout;
	}

	public void setMdPayout(Double mdPayout) {
		this.mdPayout = mdPayout;
	}

	public Double getRevenueCost() {
		return revenueCost;
	}

	public void setRevenueCost(Double revenueCost) {
		this.revenueCost = revenueCost;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getHour() {
		return hour;
	}

	public void setHour(String hour) {
		this.hour = hour;
	}

}
