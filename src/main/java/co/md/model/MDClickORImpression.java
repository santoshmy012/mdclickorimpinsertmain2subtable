package co.md.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * MDClickORImpression Entity is used to wrap all the information of the click tracker as well as impression tracker with campaign settings and much more.
 * @author Santosh
 *
 */
@Entity
@Table(name="eng_clk")
public class MDClickORImpression {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column(name="md_cid")
	private Long  mdCid;
	@Column(name="md_fid")
	private Long mdFid;
	@Column(name="md_crid")
	private Long mdCrid;
	@Column(name="md_engine_code")
	private Integer mdEngineCode;
	@Column(name="md_event_name")
	private String mdEventName;
	@Column(name="md_txn_id")
	private String mdTxId;
	@Column(name="md_datetime")
	private String mdDateTime;
	@Column(name="md_date")
	private java.sql.Date mdDate;
	@Column(name="md_hour")
	private String hour;
	@Column(name="md_wk")
	private Integer mdWeekNo;
	@Column(name="md_revenue")
	private Float mdRevenue;
	@Column(name="md_payout")
	private Float mdPayout;
	@Column(name="md_uuid")
	private String mdUUID;
	@Column(name="md_udid")
	private String mdUDID;
	@Column(name="md_imei")
	private String mdIMEINO;
	@Column(name="md_mobile_no")
	private String mdMobileNO;
	@Column(name="md_device_type")
	private String mdDeviceType;
	@Column(name="md_browser")
	private String mdBrowser;
	@Column(name="md_device_make")
	private String mdDeviceMake;
	@Column(name="md_os")
	private String mdOS;
	@Column(name="md_ip")
	private String mdIp;
	@Column(name="md_country")
	private String mdCountry;
	@Column(name="md_state")
	private String mdState;
	@Column(name="md_city")
	private String mdCity;
	@Column(name="md_carrier")
	private String mdCarrier;
	@Column(name="md_referrer")
	private String mdReferer;
	@Column(name="md_pb")
	private String mdPostBack;
	@Column(name="md_uad")
	private String userAgent;
	@Column(name="md_sub")
	private String sub1;
	@Column(name="md_sub2")
	private String sub2;
	@Column(name="md_sub3")
	private String sub3;
	@Column(name="md_sub4")
	private String sub4;
	@Column(name="md_sub5")
	private String sub5;
	@Column(name="md_sub6")
	private String sub6;
	@Column(name="md_sub7")
	private String sub7;
	@Column(name="md_sub8")
	private String sub8;
	@Column(name="md_sub9")
	private String sub9;
	@Column(name="md_sub10")
	private String sub10;
	

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getMdCid() {
		return mdCid;
	}
	public void setMdCid(Long mdCid) {
		this.mdCid = mdCid;
	}
	public Long getMdFid() {
		return mdFid;
	}
	public void setMdFid(Long mdFid) {
		this.mdFid = mdFid;
	}
	public Long getMdCrid() {
		return mdCrid;
	}
	public void setMdCrid(Long mdCrid) {
		this.mdCrid = mdCrid;
	}
	public Integer getMdEngineCode() {
		return mdEngineCode;
	}
	public void setMdEngineCode(Integer mdEngineCode) {
		this.mdEngineCode = mdEngineCode;
	}
	public String getMdEventName() {
		return mdEventName;
	}
	public void setMdEventName(String mdEventName) {
		this.mdEventName = mdEventName;
	}
	public String getMdTxId() {
		return mdTxId;
	}
	public void setMdTxId(String mdTxId) {
		this.mdTxId = mdTxId;
	}
	public String getMdDateTime() {
		return mdDateTime;
	}
	public void setMdDateTime(String mdDateTime) {
		this.mdDateTime = mdDateTime;
	}
	public java.sql.Date getMdDate() {
		return mdDate;
	}
	public void setMdDate(java.sql.Date mdDate) {
		this.mdDate = mdDate;
	}
	public String getHour() {
		return hour;
	}
	public void setHour(String hour) {
		this.hour = hour;
	}
	public Integer getMdWeekNo() {
		return mdWeekNo;
	}
	public void setMdWeekNo(Integer mdWeekNo) {
		this.mdWeekNo = mdWeekNo;
	}
	public Float getMdRevenue() {
		return mdRevenue;
	}
	public void setMdRevenue(Float mdRevenue) {
		this.mdRevenue = mdRevenue;
	}
	public Float getMdPayout() {
		return mdPayout;
	}
	public void setMdPayout(Float mdPayout) {
		this.mdPayout = mdPayout;
	}
	public String getMdUUID() {
		return mdUUID;
	}
	public void setMdUUID(String mdUUID) {
		this.mdUUID = mdUUID;
	}
	public String getMdUDID() {
		return mdUDID;
	}
	public void setMdUDID(String mdUDID) {
		this.mdUDID = mdUDID;
	}
	public String getMdIMEINO() {
		return mdIMEINO;
	}
	public void setMdIMEINO(String mdIMEINO) {
		this.mdIMEINO = mdIMEINO;
	}
	public String getMdMobileNO() {
		return mdMobileNO;
	}
	public void setMdMobileNO(String mdMobileNO) {
		this.mdMobileNO = mdMobileNO;
	}
	public String getMdDeviceType() {
		return mdDeviceType;
	}
	public void setMdDeviceType(String mdDeviceType) {
		this.mdDeviceType = mdDeviceType;
	}
	public String getMdBrowser() {
		return mdBrowser;
	}
	public void setMdBrowser(String mdBrowser) {
		this.mdBrowser = mdBrowser;
	}
	public String getMdDeviceMake() {
		return mdDeviceMake;
	}
	public void setMdDeviceMake(String mdDeviceMake) {
		this.mdDeviceMake = mdDeviceMake;
	}
	public String getMdOS() {
		return mdOS;
	}
	public void setMdOS(String mdOS) {
		this.mdOS = mdOS;
	}
	public String getMdIp() {
		return mdIp;
	}
	public void setMdIp(String mdIp) {
		this.mdIp = mdIp;
	}
	public String getMdCountry() {
		return mdCountry;
	}
	public void setMdCountry(String mdCountry) {
		this.mdCountry = mdCountry;
	}
	public String getMdState() {
		return mdState;
	}
	public void setMdState(String mdState) {
		this.mdState = mdState;
	}
	public String getMdCity() {
		return mdCity;
	}
	public void setMdCity(String mdCity) {
		this.mdCity = mdCity;
	}
	public String getMdCarrier() {
		return mdCarrier;
	}
	public void setMdCarrier(String mdCarrier) {
		this.mdCarrier = mdCarrier;
	}
	public String getMdReferer() {
		return mdReferer;
	}
	public void setMdReferer(String mdReferer) {
		this.mdReferer = mdReferer;
	}
	public String getMdPostBack() {
		return mdPostBack;
	}
	public void setMdPostBack(String mdPostBack) {
		this.mdPostBack = mdPostBack;
	}
	public String getUserAgent() {
		return userAgent;
	}
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}
	public String getSub1() {
		return sub1;
	}
	public void setSub1(String sub1) {
		this.sub1 = sub1;
	}
	public String getSub2() {
		return sub2;
	}
	public void setSub2(String sub2) {
		this.sub2 = sub2;
	}
	public String getSub3() {
		return sub3;
	}
	public void setSub3(String sub3) {
		this.sub3 = sub3;
	}
	public String getSub4() {
		return sub4;
	}
	public void setSub4(String sub4) {
		this.sub4 = sub4;
	}
	public String getSub5() {
		return sub5;
	}
	public void setSub5(String sub5) {
		this.sub5 = sub5;
	}
	public String getSub6() {
		return sub6;
	}
	public void setSub6(String sub6) {
		this.sub6 = sub6;
	}
	public String getSub7() {
		return sub7;
	}
	public void setSub7(String sub7) {
		this.sub7 = sub7;
	}
	public String getSub8() {
		return sub8;
	}
	public void setSub8(String sub8) {
		this.sub8 = sub8;
	}
	public String getSub9() {
		return sub9;
	}
	public void setSub9(String sub9) {
		this.sub9 = sub9;
	}
	public String getSub10() {
		return sub10;
	}
	public void setSub10(String sub10) {
		this.sub10 = sub10;
	}
		@Override
	public String toString() {
		return 
				"{"+getId()+","+getMdFid()+","+getMdDateTime()+","+getMdDate()+","+getHour()+","+getMdState()+","+getMdCity()+","+getMdBrowser()+","+getMdPostBack()+","+getMdCarrier()+","+getMdCid()+","+getMdEngineCode()+","+getMdEventName()+","+getMdCrid()+"}";
	}
}