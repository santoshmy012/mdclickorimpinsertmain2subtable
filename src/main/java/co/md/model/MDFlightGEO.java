package co.md.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This MDFlightGEO Entity contains the publisher information with its clicks and impressions info with GEO info
 * @author Santosh
 *
 */
@Entity
@Table(name = "report_mdflight_country_display")
public class MDFlightGEO {
	

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column(name="mdfid")
	private Long mdFid;
	@Column(name="country")
	private String 	mdCountry;
	@Column(name="state")
	private String  mdState;
	@Column(name="city")
	private String  mdCity;
	@Column(name="clicks")
	private Long mdClicks;
	@Column(name="impressions")
	private Long  mdImpressions;
	@Column(name="payout")
	private Double mdPayouts;
	@Column(name="revenuecost")
	private Double mdRevenueCost;
	@Column(name="date")
	private java.sql.Date mdDate;
	@Column(name="hour")
	private String mdHour;
	
	

		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public Long getMdFid() {
			return mdFid;
		}
		public void setMdFid(Long mdFid) {
			this.mdFid = mdFid;
		}
		public String getMdCountry() {
			return mdCountry;
		}
		public void setMdCountry(String mdCountry) {
			this.mdCountry = mdCountry;
		}
		public String getMdState() {
			return mdState;
		}
		public void setMdState(String mdState) {
			this.mdState = mdState;
		}
		public String getMdCity() {
			return mdCity;
		}
		public void setMdCity(String mdCity) {
			this.mdCity = mdCity;
		}
		public Long getMdClicks() {
			return mdClicks;
		}
		public void setMdClicks(Long mdClicks) {
			this.mdClicks = mdClicks;
		}
		public Long getMdImpressions() {
			return mdImpressions;
		}
		public void setMdImpressions(Long mdImpressions) {
			this.mdImpressions = mdImpressions;
		}
		public Double getMdPayouts() {
			return mdPayouts;
		}
		public void setMdPayouts(Double mdPayouts) {
			this.mdPayouts = mdPayouts;
		}
		public Double getMdRevenueCost() {
			return mdRevenueCost;
		}
		public void setMdRevenueCost(Double mdRevenueCost) {
			this.mdRevenueCost = mdRevenueCost;
		}
		public java.sql.Date getMdDate() {
			return mdDate;
		}
		public void setMdDate(java.sql.Date mdDate) {
			this.mdDate = mdDate;
		}
		public String getMdHour() {
			return mdHour;
		}
		public void setMdHour(String mdHour) {
			this.mdHour = mdHour;
		}
					
}
