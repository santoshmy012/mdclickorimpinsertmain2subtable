package co.md.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.md.model.MDFlightBrowser;
import co.md.repository.MDFlightBrowserRepository;

@Service("mDFlightBrowserService")
public class MDFlightBrowserServiceImpl implements MDFlightBrowserService {

	@Autowired
	private MDFlightBrowserRepository mdFlightBrowserRepository; 
	@Override
	public MDFlightBrowser saveOrUpdate(MDFlightBrowser mdFlightBrowser) {
		return mdFlightBrowserRepository.saveAndFlush(mdFlightBrowser);
	}

}
