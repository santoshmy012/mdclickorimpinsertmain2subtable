package co.md.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.md.model.MDFlightSource;
import co.md.repository.MDFlightSourceRepository;

@Service("MDFlightSourceService")
public class MDFlightSourceServiceImpl implements MDFlightSourceService {
	
	@Autowired
	private MDFlightSourceRepository mdFlightSourceRepository;
	
	@Override
	public MDFlightSource saveOrUpdate(MDFlightSource mdFlightSource) {
		return mdFlightSourceRepository.saveAndFlush(mdFlightSource);
	}

}
