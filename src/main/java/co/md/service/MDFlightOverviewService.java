package co.md.service;

import co.md.model.MDFlightOverview;

public interface MDFlightOverviewService {
	MDFlightOverview saveOrUpdate(MDFlightOverview mdFlightOverview);
}
