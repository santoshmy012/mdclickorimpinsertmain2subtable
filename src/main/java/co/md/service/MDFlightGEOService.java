package co.md.service;

import co.md.model.MDFlightGEO;

/**
 * This Service only describe the GEO service for publisher only
 * @author Santosh
 *
 */
public interface MDFlightGEOService {
	MDFlightGEO saveOrUpdate(MDFlightGEO mdFlightGEO);
}
