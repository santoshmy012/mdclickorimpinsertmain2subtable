package co.md.service;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.md.model.MDClickORImpression;
import co.md.repository.MDClickOrImpressionRepository;

@Service("mDClickOrImpressionService")
public class MDClickOrImpressionServiceImpl implements MDClickOrImpressionService {
	@Autowired
	private MDClickOrImpressionRepository mDClickOrImpressionRepository;
	@Override
	public List<MDClickORImpression> listMDClickOrImpressions(){
		return mDClickOrImpressionRepository.findAll();
	}
	
}
