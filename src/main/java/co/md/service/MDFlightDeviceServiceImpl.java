package co.md.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.md.model.MDFlightDevice;
import co.md.repository.MDFlightDeviceRepository;

@Service("MDFlightDeviceService")
public class MDFlightDeviceServiceImpl implements MDFlightDeviceService {
	
	@Autowired
	private MDFlightDeviceRepository mdFlightDeviceRepository;
	@Override
	public MDFlightDevice saveOrUpdate(MDFlightDevice mdFlightDevice) {
		return mdFlightDeviceRepository.saveAndFlush(mdFlightDevice);
	}

}
