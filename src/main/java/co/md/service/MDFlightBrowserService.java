package co.md.service;

import co.md.model.MDFlightBrowser;

public interface MDFlightBrowserService {
	MDFlightBrowser saveOrUpdate(MDFlightBrowser mdFlightBrowser);
}
