package co.md.service;

import co.md.model.MDFlightDevice;

public interface MDFlightDeviceService {
	MDFlightDevice saveOrUpdate(MDFlightDevice mdFlightDevice);
}
