package co.md.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.md.model.MDFlightOS;
import co.md.repository.MDFlightOSRepository;

@Service("MDFlightOSService")
public class MDFlightOSServiceImpl implements MDFlightOSService {

	@Autowired
	private MDFlightOSRepository mdFlightOSRepositry;
	@Override
	public MDFlightOS saveOrUpdate(MDFlightOS mdFlightOS) {
		return mdFlightOSRepositry.saveAndFlush(mdFlightOS);
	}

}
