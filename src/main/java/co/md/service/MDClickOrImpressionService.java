package co.md.service;

import java.util.List;

import co.md.model.MDClickORImpression;

public interface MDClickOrImpressionService {
	public List<MDClickORImpression> listMDClickOrImpressions();
}
