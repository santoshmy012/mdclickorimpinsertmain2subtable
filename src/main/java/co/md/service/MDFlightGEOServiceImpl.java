package co.md.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.md.model.MDFlightGEO;
import co.md.repository.MDFlightGEORepository;

@Service("mDFlightGEOService")
public class MDFlightGEOServiceImpl implements MDFlightGEOService {
	
	@Autowired
	private MDFlightGEORepository mDFlightGEORepository;
	@Override
	public MDFlightGEO saveOrUpdate(MDFlightGEO mdFlightGEO) {
		return mDFlightGEORepository.saveAndFlush(mdFlightGEO) ;
	}

}
