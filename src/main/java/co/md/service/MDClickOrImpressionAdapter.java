package co.md.service;

import java.sql.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import co.md.model.MDFlightBrowser;
import co.md.model.MDFlightCreative;
import co.md.model.MDFlightDevice;
import co.md.model.MDFlightGEO;
import co.md.model.MDFlightOS;
import co.md.model.MDFlightOverview;
import co.md.model.MDFlightSource;
import co.md.repository.MDFlightOSRepository;

@Component
public class MDClickOrImpressionAdapter {

	@Autowired
	private EntityManager entityManager;// This is used to create the custom
										// Query
	@Autowired
	private MDFlightGEOService mdFlightGEOService;
	@Autowired
	private MDFlightBrowserService mdFlightBrowserService;

	@Autowired
	private MDFlightCreativeService mdFlightCreativeService;
	@Autowired
	private MDFlightDeviceService mdFlightDeviceService;
	@Autowired
	private MDFlightOSService mdFlightOSService;
	@Autowired
	private MDFlightOverviewService mdFlightOverviewService;
	@Autowired
	private MDFlightSourceService mdFlightSourceService;
	

	/*
	 * This function is used to get the flight details upon eng_clk table and
	 * get data such as city, country, date, hour, engine code(1 as impression
	 * and 2 as click) ,total revenue, total payout. After getting all the above
	 * parameter it simply dumps into the another table using the
	 * MDFlightGEOService component
	 */

	public void saveClickOrImpParams2ReporyFlighCountry() {
		// step 1 create Manual Query using entity manager which return the
		// Query object
		Query query = entityManager.createQuery(
				"SELECT m.mdFid, m.mdCity, m.mdState, m.mdCountry, m.mdDate , m.hour , m.mdEngineCode ,  count(m.mdEngineCode) , sum(m.mdRevenue), sum(m.mdPayout) from MDClickORImpression m group by m.mdFid,m.mdDate , m.hour, m.mdState, m.mdCountry, m.mdCity");
		// step 2 call getResultList() method to get List of Object array
		List<Object[]> objectList = query.getResultList();
		// ste3 traverse this array and get the value using index
		objectList.forEach((item) -> {
			Long impressions = 0L, clicks = 0L;
			Long mdFlightId = (Long) item[0];
			String mdCity = (String) item[1];
			String mdState = (String) item[2];
			String mdCountry = (String) item[3];
			java.sql.Date mdDate = (java.sql.Date) item[4];
			String mdHour = (String) item[5];
			Integer mdEngineCode = (Integer) item[6];
			Long mdEngineCodeCount = (Long) item[7];
			Double mdRevenue = (Double) item[8];
			Double mdPayout = (Double) item[9];

			// step 4 create MDFlightGEO(Persistent Entity)
			MDFlightGEO mdFlightGEO = new MDFlightGEO();

			// step 5 set all above parameters to this object
			mdFlightGEO.setMdFid(mdFlightId);
			mdFlightGEO.setMdCity(mdCity);
			mdFlightGEO.setMdState(mdState);
			mdFlightGEO.setMdCountry(mdCountry);
			mdFlightGEO.setMdDate(mdDate);
			mdFlightGEO.setMdHour(mdHour);
			if (mdEngineCode == 1)
				impressions = mdEngineCodeCount;
			if (mdEngineCode == 2)
				clicks = mdEngineCodeCount;
			// set this code to the Bean
			mdFlightGEO.setMdClicks(clicks);
			mdFlightGEO.setMdImpressions(impressions);
			mdFlightGEO.setMdPayouts(mdPayout);
			mdFlightGEO.setMdRevenueCost(mdRevenue);

			// step 5 finally save this object by calling saveOrUpdate of
			// MDFlightGEOService interface, This interface save this object
			// permanently to your database
			MDFlightGEO flightGEOObject = mdFlightGEOService.saveOrUpdate(mdFlightGEO);
		});

	}

	public void saveClickOrImpParams2ReportFlightBrowser() {

		// step 1 create Manual Query using entity manager which return the
		// Query object
		Query query = entityManager.createQuery(
				"SELECT m.mdFid, m.mdBrowser, m.mdEngineCode, count(m.mdEngineCode),sum(m.mdPayout), sum(m.mdRevenue), m.mdDate, m.hour from  MDClickORImpression m group by m.mdFid, m.mdDate, m.hour, m.mdBrowser");
		// step 2 call getResultList() method to get List of Object array
		List<Object[]> objectList = query.getResultList();
		// ste3 traverse this array and get the value using index and set this
		// value to the model object
		objectList.forEach((item) -> {
			Long impressions = 0L, clicks = 0L;
			Long mdFlightId = (Long) item[0];
			String mdBrowser = (String) item[1];
			Integer mdEngineCode = (Integer) item[2];
			Long mdEngineCodeCount = (Long) item[3];
			Double mdPayout = (Double) item[4];
			Double mdRevenue = (Double) item[5];
			java.sql.Date mdDate = (Date) item[6];
			String mdHour = (String) item[7];

			MDFlightBrowser mdFlightCreative = new MDFlightBrowser();
			// now set all values to the this model
			if (mdEngineCode == 1)
				impressions = mdEngineCodeCount;
			if (mdEngineCode == 2)
				clicks = mdEngineCodeCount;

			mdFlightCreative.setMdFId(mdFlightId);
			mdFlightCreative.setMdBrowser(mdBrowser);
			mdFlightCreative.setDate(mdDate);
			mdFlightCreative.setHour(mdHour);
			mdFlightCreative.setMdPayout(mdPayout);
			mdFlightCreative.setRevenueCost(mdRevenue);
			mdFlightCreative.setMdClicks(clicks);
			mdFlightCreative.setMdImpressions(impressions);

			// step 4 finally
			mdFlightBrowserService.saveOrUpdate(mdFlightCreative);

		});
	}

	public void saveClickOrImpParams2ReportFlightCreative() {

		// step 1 create Manual Query using entity manager which return the
		// Query object
		Query query = entityManager.createQuery(
				"SELECT m.mdFid, m.mdCrid, m.mdEngineCode, count(m.mdEngineCode),sum(m.mdPayout), sum(m.mdRevenue), m.mdDate, m.hour from  MDClickORImpression m group by m.mdFid,m.mdCrid, m.mdDate, m.hour");
		// step 2 call getResultList() method to get List of Object array
		List<Object[]> objectList = query.getResultList();
		// ste3 traverse this array and get the value using index and set this
		// value to the model object
		objectList.forEach((item) -> {

			Long impressions = 0L, clicks = 0L;
			// flight id
			Long mdFlightId = (Long) item[0];
			// creative id
			Long mdCreativeId = (Long) item[1];
			Integer mdEngineCode = (Integer) item[2];
			Long mdEngineCodeCount = (Long) item[3];

			Double mdPayout = (Double) item[4];
			Double mdRevenue = (Double) item[5];
			java.sql.Date mdDate = (Date) item[6];
			String mdHour = (String) item[7];

			MDFlightCreative mdFlightCreative = new MDFlightCreative();
			// now set all values to the this model
			if (mdEngineCode == 1)
				impressions = mdEngineCodeCount;
			if (mdEngineCode == 2)
				clicks = mdEngineCodeCount;

			mdFlightCreative.setMdFId(mdFlightId);
			mdFlightCreative.setDate(mdDate);
			mdFlightCreative.setHour(mdHour);
			mdFlightCreative.setMdPayout(mdPayout);
			mdFlightCreative.setRevenueCost(mdRevenue);
			mdFlightCreative.setMdClicks(clicks);
			mdFlightCreative.setMdImpressions(impressions);

			// step 4 finally save this creative into the database
			mdFlightCreativeService.saveOrUpdate(mdFlightCreative);

		});

	}

	public void saveClickOrImpParams2ReportFlightDevice() {
		// STEP 1 CREATE MANULA SQL QUERY USING ENTITYMANAGER GIVEN BY JPA, WILL
		// RETURN QUERY OBJECT
		Query query = entityManager.createQuery(
				"SELECT m.mdFid, m.mdDeviceMake, m.mdEngineCode, count(m.mdEngineCode),sum(m.mdPayout), sum(m.mdRevenue), m.mdDate, m.hour from  MDClickORImpression m group by m.mdFid,m.mdDeviceMake, m.mdDate, m.hour");
		// STEP 2 INVOKE getResultList() ON QUERY OBJECT TO GET THE RESULTSET AS
		// A LIST OF OBJECTS
		List<Object[]> objectList = query.getResultList();
		// ste3 traverse this array and get the value using index and set this
		// value to the model object
		objectList.forEach((item) -> {

			Long impressions = 0L, clicks = 0L;
			// GET FLIGHT ID
			Long mdFlightId = (Long) item[0];
			// GET DEVICE MAKE
			String deviceMake = (String) item[1];
			// GET MDENGINE CODE
			Integer mdEngineCode = (Integer) item[2];
			// GET MDENGINE COUNT
			Long mdEngineCodeCount = (Long) item[3];
			// GET MDPAYOUT
			Double mdPayout = (Double) item[4];
			// GET REVENUE
			Double mdRevenue = (Double) item[5];
			// GET DATE
			java.sql.Date mdDate = (Date) item[6];
			// GET HOUR
			String mdHour = (String) item[7];
			// CREATE ENTITY OBJECT
			MDFlightDevice mdFlightDevice = new MDFlightDevice();
			// SET ALL PARAMETER VALUES TO THE ENTITY OBJECT
			if (mdEngineCode == 1)
				impressions = mdEngineCodeCount;
			if (mdEngineCode == 2)
				clicks = mdEngineCodeCount;

			mdFlightDevice.setMdFId(mdFlightId);
			mdFlightDevice.setMdDevice(deviceMake);
			mdFlightDevice.setDate(mdDate);
			mdFlightDevice.setHour(mdHour);
			mdFlightDevice.setMdPayout(mdPayout);
			mdFlightDevice.setRevenueCost(mdRevenue);
			mdFlightDevice.setMdClicks(clicks);
			mdFlightDevice.setMdImpressions(impressions);

			// STEP 4 FINALLY SAVE THIS ENTITY OBJECT INTO THE DATABASE
			mdFlightDeviceService.saveOrUpdate(mdFlightDevice);

		});

	}

	public void saveClickOrImpParams2ReportFlightOS() {

		// STEP 1 CREATE MANULA SQL QUERY USING ENTITYMANAGER GIVEN BY JPA, WILL
		// RETURN QUERY OBJECT
		Query query = entityManager.createQuery(
				"SELECT m.mdFid, m.mdOS, m.mdEngineCode, count(m.mdEngineCode),sum(m.mdPayout), sum(m.mdRevenue), m.mdDate, m.hour from  MDClickORImpression m group by m.mdFid,m.mdOS, m.mdDate, m.hour");
		// STEP 2 INVOKE getResultList() ON QUERY OBJECT TO GET THE RESULTSET AS
		// A LIST OF OBJECTS
		List<Object[]> objectList = query.getResultList();
		// ste3 traverse this array and get the value using index and set this
		// value to the model object
		objectList.forEach((item) -> {

			Long impressions = 0L, clicks = 0L;
			// GET FLIGHT ID
			Long mdFlightId = (Long) item[0];
			// GET DEVICE OS
			String mdOs = (String) item[1];
			// GET MDENGINE CODE
			Integer mdEngineCode = (Integer) item[2];
			// GET MDENGINE COUNT
			Long mdEngineCodeCount = (Long) item[3];
			// GET MDPAYOUT
			Double mdPayout = (Double) item[4];
			// GET REVENUE
			Double mdRevenue = (Double) item[5];
			// GET DATE
			java.sql.Date mdDate = (Date) item[6];
			// GET HOUR
			String mdHour = (String) item[7];
			// CREATE ENTITY OBJECT
			MDFlightOS mdFliightOS = new MDFlightOS();
			// SET ALL PARAMETER VALUES TO THE ENTITY OBJECT
			if (mdEngineCode == 1)
				impressions = mdEngineCodeCount;
			if (mdEngineCode == 2)
				clicks = mdEngineCodeCount;

			mdFliightOS.setMdFId(mdFlightId);
			mdFliightOS.setMdOS(mdOs);
			mdFliightOS.setDate(mdDate);
			mdFliightOS.setHour(mdHour);
			mdFliightOS.setMdPayout(mdPayout);
			mdFliightOS.setRevenueCost(mdRevenue);
			mdFliightOS.setMdClicks(clicks);
			mdFliightOS.setMdImpressions(impressions);

			// STEP 4 FINALLY SAVE THIS ENTITY OBJECT INTO THE DATABASE
			mdFlightOSService.saveOrUpdate(mdFliightOS);

		});
	}
	
	
	public void saveClickOrImpParams2ReportFlightOverview(){



		// STEP 1 CREATE MANULA SQL QUERY USING ENTITYMANAGER GIVEN BY JPA, WILL
		// RETURN QUERY OBJECT
		Query query = entityManager.createQuery(
				"SELECT m.mdFid, m.mdEngineCode, count(m.mdEngineCode),sum(m.mdPayout), sum(m.mdRevenue), m.mdDate, m.hour from  MDClickORImpression m group by m.mdFid, m.mdDate, m.hour");
		// STEP 2 INVOKE getResultList() ON QUERY OBJECT TO GET THE RESULTSET AS
		// A LIST OF OBJECTS
		List<Object[]> objectList = query.getResultList();
		// ste3 traverse this array and get the value using index and set this
		// value to the model object
		objectList.forEach((item) -> {

			Long impressions = 0L, clicks = 0L;
			// GET FLIGHT ID
			Long mdFlightId = (Long) item[0];
			// GET MDENGINE CODE
			Integer mdEngineCode = (Integer) item[1];
			// GET MDENGINE COUNT
			Long mdEngineCodeCount = (Long) item[2];
			// GET MDPAYOUT
			Double mdPayout = (Double) item[3];
			// GET REVENUE
			Double mdRevenue = (Double) item[4];
			// GET DATE
			java.sql.Date mdDate = (Date) item[5];
			// GET HOUR
			String mdHour = (String) item[6];
			// CREATE ENTITY OBJECT
			MDFlightOverview mdFlightOverview = new MDFlightOverview();
			// SET ALL PARAMETER VALUES TO THE ENTITY OBJECT
			if (mdEngineCode == 1)
				impressions = mdEngineCodeCount;
			if (mdEngineCode == 2)
				clicks = mdEngineCodeCount;

			mdFlightOverview.setMdFId(mdFlightId);
			mdFlightOverview.setDate(mdDate);
			mdFlightOverview.setHour(mdHour);
			mdFlightOverview.setMdPayout(mdPayout);
			mdFlightOverview.setRevenueCost(mdRevenue);
			mdFlightOverview.setMdClicks(clicks);
			mdFlightOverview.setMdImpressions(impressions);

			// STEP 4 FINALLY SAVE THIS ENTITY OBJECT INTO THE DATABASE
			mdFlightOverviewService.saveOrUpdate(mdFlightOverview);

		});
	
	}
	
	public void saveClickOrImpParams2ReportFlightSource(){



		// STEP 1 CREATE MANULA SQL QUERY USING ENTITYMANAGER GIVEN BY JPA, WILL
		// RETURN QUERY OBJECT
		Query query = entityManager.createQuery("SELECT m.mdFid, m.mdReferer, mdEngineCode, count(m.mdEngineCode),sum(m.mdPayout), sum(m.mdRevenue), m.mdDate, m.hour from  MDClickORImpression m group by m.mdFid,m.mdReferer, m.mdDate, m.hour");
		// STEP 2 INVOKE getResultList() ON QUERY OBJECT TO GET THE RESULTSET AS
		// A LIST OF OBJECTS
		List<Object[]> objectList = query.getResultList();
		// ste3 traverse this array and get the value using index and set this
		// value to the model object
		objectList.forEach((item) -> {

			Long impressions = 0L, clicks = 0L;
			// GET FLIGHT ID
			Long mdFlightId = (Long) item[0];
			String mdReferer = (String) item[1];
			
			// GET MDENGINE CODE
			Integer mdEngineCode = (Integer) item[2];
			// GET MDENGINE COUNT
			Long mdEngineCodeCount = (Long) item[3];
			// GET MDPAYOUT
			Double mdPayout = (Double) item[4];
			// GET REVENUE
			Double mdRevenue = (Double) item[5];
			// GET DATE
			java.sql.Date mdDate = (Date) item[6];
			// GET HOUR
			String mdHour = (String) item[7];
			// CREATE ENTITY OBJECT
			MDFlightSource mdFlightSource = new MDFlightSource();
			// SET ALL PARAMETER VALUES TO THE ENTITY OBJECT
			if (mdEngineCode == 1)
				impressions = mdEngineCodeCount;
			if (mdEngineCode == 2)
				clicks = mdEngineCodeCount;

			mdFlightSource.setMdFId(mdFlightId);
			mdFlightSource.setDate(mdDate);
			mdFlightSource.setHour(mdHour);
			mdFlightSource.setMdPayout(mdPayout);
			mdFlightSource.setRevenueCost(mdRevenue);
			mdFlightSource.setMdClicks(clicks);
			mdFlightSource.setMdImpressions(impressions);

			// STEP 4 FINALLY SAVE THIS ENTITY OBJECT INTO THE DATABASE
			mdFlightSourceService.saveOrUpdate(mdFlightSource);

		});
	
	}
	

}
