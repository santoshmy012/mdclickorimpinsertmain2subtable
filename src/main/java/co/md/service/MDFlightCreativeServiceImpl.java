package co.md.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.md.model.MDFlightCreative;
import co.md.repository.MDFlightCreativeRepository;

@Service("MDFlightCreativeService")
public class MDFlightCreativeServiceImpl  implements MDFlightCreativeService{

	
	@Autowired
	private MDFlightCreativeRepository mdFlightCreativeRepository;
	
	@Override
	public MDFlightCreative saveOrUpdate(MDFlightCreative mdFlightCreative) {
		return mdFlightCreativeRepository.saveAndFlush(mdFlightCreative);
	}

}
