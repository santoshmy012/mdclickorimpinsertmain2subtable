package co.md.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.md.model.MDFlightOverview;
import co.md.repository.MDFlightOverviewRepository;

@Service("MDFlightOverviewService")
public class MDFlightOverviewServiceImpl implements MDFlightOverviewService {

	@Autowired
	private MDFlightOverviewRepository mdFlightOverviewRepository; 
	@Override
	public MDFlightOverview saveOrUpdate(MDFlightOverview mdFlightOverview) {
		return mdFlightOverviewRepository.saveAndFlush(mdFlightOverview);
	}

}
